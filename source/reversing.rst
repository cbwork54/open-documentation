===================
Reverse Engineering
===================

Reversing a new executable from scratch can be a tough challenge. The community around the game has become very closed regarding information. This page is for sharing offsets.

Where to get info?
==================

1. Look on the net. There is plenty of info in existing code.
2. Look at the binary. Many debug strings can tell you what the code does.
3. Look at the binary, again. Some may contain runtime type information (RTTI). This will tell you class names, inheritance and (virtual) member functions.
4. Ask. Just ask. If you form a polite, well asked question, you will most-likeley get an answer.
5. If you're really desperate: Compile a test-binary with the same compiler and stdlib and use tools like K4m1n0 to find similarities.
6. Still no luck? ... dig **deep** into the binary ... some functions may remain unknown for days, weeks, even months.

Giveaways
=========

Offsets
-------

Some addresses to start off.

+----------------+----------------------------------------------------------------+--------+---------------+
| Offset         | Description                                                    | Type   | Client        |
+================+================================================================+========+===============+
| ``0xDEADBEEF`` | Example data offset that does nothing                          | data   | VSRO 188      |
+----------------+----------------------------------------------------------------+--------+---------------+
| ``0x008311C0`` | WndProc                                                        | code   | VSRO 188      |
+----------------+----------------------------------------------------------------+--------+---------------+
| ``0x008328C0`` | WinMain                                                        | code   | VSRO 188      |
+----------------+----------------------------------------------------------------+--------+---------------+
| ``0x009EC570`` | CNavigationDeadreckon::MoveToCoords(short, struct D3DVECTOR *) | code   | VSRO 188      |
+----------------+----------------------------------------------------------------+--------+---------------+
| ``0x009D87C0`` | CICPlayer::Render                                              | code   | VSRO 188      |
+----------------+----------------------------------------------------------------+--------+---------------+
| ``0x00BAD750`` | CGFXVideo3d::EndScene                                          | code   | VSRO 188      |
+----------------+----------------------------------------------------------------+--------+---------------+
| ``0x00BAED40`` | CGFXVideo3d::BeginScene                                        | code   | VSRO 188      |
+----------------+----------------------------------------------------------------+--------+---------------+

Structures
----------

Some reverse engineered structures to play around in ReClass.


.. code-block:: cpp
	:linenos:

	// This camera class is a demonstration, it's not the real one
	struct CCamera
	{
		D3DVECTOR position;
		D3DVECTOR rotation;
	}

