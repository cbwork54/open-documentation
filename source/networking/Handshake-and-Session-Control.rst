===========================
Handshake & Session Control
===========================

The very first messages after opening the TCP connection are the handshake. The server also describes these as *session*. That's why this article is also titled *Session Control*. In order to communicate with a server or a client, we need to open a session.

The server sends the first message after the TCP connection is established. Depending on the selected security features, the client has to send a proper answer.

Initialze a session
===================


+-------------------+----------+
| Type              | Bit      |
+===================+==========+
| None              | 00000000 |
+-------------------+----------+
| Disabled          | 00000001 |
+-------------------+----------+
| Blowfish          | 00000010 |
+-------------------+----------+
| SecurityBytes     | 00000100 |
+-------------------+----------+
| Handshake         | 00001000 |
+-------------------+----------+
| HandshakeResponse | 00010000 |
+-------------------+----------+

**Remark**: *The three remaining bits are unused.*
