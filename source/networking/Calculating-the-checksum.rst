========================
Calculating the checksum
========================

Algorithm
=========

The game's checksum calculation is just a generic CRC32 checksum algorithm. It's the *fast* implementation because it uses a pre-calculated table. 

Usually, the table is generated from a *fixed* poly. In our case, the poly is selected by the server from a list of 256 different polys. The selected index of the poly in the list is transmitted in the first handshake message.

Operation
=========

* The checksum is generated before encryption. 
* The checksum includes all bytes of the message. 
* Since the checksum byte is part of the calculation, it's treated as 0 for the calculation.


Trivia
======

* Properitary multi-connection endpoints will not reuse already generated tables
* Some third-party implementations just ship all 256 tables (65.536 bytes) pre-generated as code.

