===================
Connection dataflow
===================

Establishing a connection
=========================

In order to establish a new connection, two major things must be done aside from opening a TCP connection.

Handshake
---------

The :doc:`messages/global/5000_HANDSHAKE` exchanges security settings and parameters. The first packet is always send from the server-side and contains information about which securtiy settings to enable and which initial parameters to use. Depeding on which settings are enabled, a Diffie-Hellman keyexchange and a challenge are performed.

If the handshake was successful, the client-side sends the :doc:`messages/global/9000_ESTABLISHED` message. Any error will lead to an immediate termination of the TCP connection.

Read more in: :doc:`Handshake-and-Session-Control`.

Identification
--------------

Once the session is established, the local and remote-side exchange their identities using :doc:`messages/global/2001_IDENTIFICATION`. The server sends it's identity first. The client has to answer with it's identity. Any unexpected data or invalid identies will lead to an immediate termination of the TCP connection.

Read more in: :doc:`Module-Identification`.


Maintaining a connection
========================

Keeping a connection alive is easy. The client has to send a message at least every 5 seconds. It doesn't matter which message is. When no messages are received by the server for 5 seconds, the server will terminate the connection. The server will not try to keep the connection alive in any way. When the client has no message to send, it sends a :doc:`messages/global/2002_PING`.

Closing a connection
====================

There is no known message to end a connection properly (to be checked).
