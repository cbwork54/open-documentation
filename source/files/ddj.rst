========================
Direct Draw Image (.ddj)
========================


This format is very simple. The whole file looks like this:

.. code-block:: cpp

	struct DDJ 
	{
	    Header header;
	    DDS dds_image;
	}
..

The header is:

.. code-block:: cpp

	// Size = 20 bytes
	struct Header 
	{
	    char[12] magic;
	    int unknown1;
	    int unknown2;
	}
..

Since the header does not contain useful information, you can just skip the first 20 bytes and load the remaining bytes as a Direct Draw Surface.

