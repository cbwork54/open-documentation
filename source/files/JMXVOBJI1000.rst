============
JMXVOBJI1000
============

+---------+-----------+
| Region  | Version   |
+=========+===========+
| Unknown | Open Beta |
+---------+-----------+

CSV-style list with spaces (``' '``) as separator. Strings need to be encapsulated in double quotes (``"my string"``)

::

	JMXVOBJI1000
	// <Number of lines>
	// <First line>
	// <Second line>
	// ...
	// <nth line>

Example

::

	JMXVOBJI1000
	16
	00000 0x00000001 "res\bldg\china\cj_ferry\cj_ferry_buil.bsr"
	00001 0x00000000 "res\bldg\china\cj_ferry\cj_ferry_warehou.bsr"
	00002 0x00000000 "res\bldg\china\cj_ferry\cj_ferry_dam02.bsr"
	00003 0x00000000 "res\bldg\china\dunhuang\ferry\naruter_buil.bsr"
	00004 0x00000000 "res\bldg\china\cj_ferry\cj_ferry_stone01.bsr"
	00005 0x00000000 "res\bldg\china\cj_ferry\cj_ferry_box.bsr"
	00006 0x00000000 "res\bldg\china\cj_ferry\cj_ferry_wagon.bsr"
	00007 0x00000001 "res\bldg\china\cj_ferry\cj_ferry_enter.bsr"
	00008 0x00000000 "res\bldg\china\cj_ferry\cj_ferry_tombstone.bsr"
	00009 0x00000000 "res\bldg\china\cj_ferry\cj_ferry_buil02.bsr"
	00010 0x00000000 "res\bldg\china\cj_ferry\cj_ferry_bottle.bsr"
	00011 0x00000001 "res\bldg\china\dunhuang\ferry\naruter_fish01.bsr"
	00012 0x00000000 "res\nature\china\dunhuang\ferry\w_cd_budawal.bsr"
	00013 0x00000001 "res\nature\china\dunhuang\ferry\w_cd_stonwal04.bsr"
	00014 0x00000000 "res\nature\china\dunhuang\ferry\w_cd_stonwal11.bsr"
	00015 0x00000000 "res\nature\china\dunhuang\ferry\w_cd_small_stonwal.bsr"
