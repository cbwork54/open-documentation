===============
Skeleton (.bsk)
===============

Skeleton of an object. Basically a weighted mapping of vertices to bones.

.. toctree::
	:maxdepth: 1

	JMXVBSK_0101
..
