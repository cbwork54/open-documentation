===============
Resource (.bsr)
===============

A resource combines :doc:`JMXVBAN`, :doc:`JMXVBMS`, :doc:`JMXVBMT` and :doc:`JMXVBSK` to an usable ingame object.

.. toctree::
	:maxdepth: 1

	JMXVRES_0109
..


Trivia
======

* The resource loader of the server requires all data files, not just the ones required for collision detection.
