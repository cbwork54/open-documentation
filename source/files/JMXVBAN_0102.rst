============
JMXVBAN 0102
============

Relevant for:

+---------+---------+
| Region  | Version |
+=========+=========+
| Vietnam | 188     |
+---------+---------+

.. code-block:: cpp
    :linenos:

    12 byte header;
    int unk;
    int unk ;
    string name;                    //name of the animation
    int duration;
    int framesPerSecond;
    int unk;
    int keyframeCount;
    int[keyframeCount] keyframes;   // timings of the keyframes, so you can interpolate between two poses.
    int boneCount;                  // Amount of bones that have transformations that are diffrent from their bind poses.
    for(int i = 0; i < boneCount;i++)
    {
       string boneName;
       int count; // same as keyframeCount
       for(int p = 0; p < count;p++)
       {
           Quaternion rotation; 
           Vector3 translation; 
                                    //These two together give you the transformation Matrix relative to it's partent bone/joint.
       }
    }
    //End of file
..
