================
Animation (.ban)
================

Stores one animation of a resource.


Versions:

.. toctree::
    :maxdepth: 1

    JMXVBAN_0102
..
