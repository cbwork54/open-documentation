=================
Recommended Tools
=================

x64dbg
======

Website: https://x64dbg.com/

A great debugger and disassembler. Has some bugs here and there but is really powerful. 


CheatEngine
===========

Website: https://www.cheatengine.org/

Classic tool for memory scanning. Alternatives?

ReClass
=======

Website: *many*

There are many *flavors* of ReClass. Pick the one that fits your needs. If you're unsure which one to choose, just pick the original one.


NTCore Explorer Suite
=====================

Website: https://ntcore.com/?page_id=388

NTCore got a great set of tools. CFF Explorer is really useful for making changes to the PE Header.


ResHacker
=========

Website: *many*

Oldschool Resource Editor. The resources of a portable executable are underestimated. Aside from the application icon, resources can also contain strings, images, forms or even large binary objects.

Interactive Disassembler
========================

Website: https://www.hex-rays.com/

IDA is a really powerful binary analysis tool. It's definitely the right tool for the job, even if it has some flaws. IDA v7.0 was released as a freeware recently, with some limitations of course, so give it a try.




