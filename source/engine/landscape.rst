=========
Landscape
=========

The landscape is divided in two major types: *Terrain* and *Dungeon*. Terrain-landscapes are mainly supplied by the :doc:`../files/JMXVNVM` for collision and :doc:`../files/JMXVMAPM`, :doc:`../files/JMXVMAPO` and :doc:`../files/t` for rendering. Usually all regions that have a sky are *Terrain*-typed. *Dungeon*-types are made of :doc:`../files/JMXVDOF`. You may know the typical dungeons e.g. Donwhang Cave or Jangan Cave, but also Capture the Flag is a dungeon.

The client will show only two dimensional coordinates. But since this is a 3D game, it has to be handled different. Positioning on the map is split into regions and relative coordinates. It's like having a usual map with these squares used for roughly finding a location and using millimeters from the top and left to specify an exact location.

The region is mostly represented as one number, but it's actually two different numbers. You need to display it in hexa-decimal notation, it wont make sense otherwise.

``region = 0x6A35`` would be ``region.X = 0x35`` and ``region.Y = 0x6A``.

The first bit of the region is reserved for choosing between *Terrain* and *Dungeon* type. The first bit is generally also known as the sign bit as it tells if a number is negative. So if you see a negative number for the region, you can tell it's inside a dungeon.

